==================================
Making the RBE Journal using LaTeX
==================================
Amy Hendrickson, TeXnology Inc,
http://www.texnology.com
amyh@texnology.com
Mar 5, 2009
----------------------------------

Files in this package:
======================
rbe-base.zip includes

==For typesetting articles:
rbe.cls

==For typesetting summary page:
summary.tex

==For typesetting cover:
rbecover.tex

==Template file for article:
rbe-tmpl.tex

==Template file for root file:
rbe-root.tex

(rbe-root.tex makes note of the
\usepackage commands that are found
in rbe.cls so that the user doesn't
bring any in a second time.)

==Current information.
You need to edit this file to supply
current information:

currinfo.tex

==For bibliography styles:
rbe-en.bst %% for english bibliography
rbe-pt.bst %% for portugeuse bibliography

==For pdfs used in design:
\imagens.zip

=============================================
Sample Files-- Full Issue, Dec 2008

==For sample .tex, .bib, and .bbl files
\sampfile.zip

==For sample illustrations
\figuras.zip

==Sample Result Files:
rbe-root.pdf %% sample set of articles
summary.pdf %% summary produced using summary.sum
rbecover.pdf %% cover including article titles

====================
====================

Steps to be taken to make journal:
(Below this section is information on making
single article)
+++++++++++++++++++++++++++++++++++++++++++++++++
1) Make directory for this issue
\currissue

Extract files from \base.zip into
the \currissue directory

Extract files from \sampfile.zip into
the \currissue directory

Make directory \currissue\images
Extract files from imagens.zip into
the \images directory

Make directory \currissue\figuras
Extract files from figuras.zip and drop them into
\figuras directory

+++++++++++++++++++++++++++++++++++++++++++++++++
2) Edit the currinfo.tex file
to supply the following information:


\setcounter{volume}{?}
\setcounter{number}{?}
\newcommand{\data}{?}

(for example)
\setcounter{volume}{62}
\setcounter{number}{4}
\newcommand{\data}{Out-Dez 2008}

+++++++++++++++++++++++++++++++++++++++++++++++++
3) Edit each article to be sure that
it contains the following commands.
Get rid of \begin{document}...\end{document},
copy any \usepackage commands to rbe-root,
and delete them from the article.tex file

%%%%%%%%%%%%%%%%%%%%%%%
\begin{article}
\currlanguage{portuges} or
\currlanguage{english} at the top of each the file

\title{}
\author{}
\maketitle

% for running heads:
\headerauthor{}
\headertitle{}

\tableofcontents

\begin{palavraschave}
...
\end{palavraschave}

\begin{classificacao}
...
\end{classificacao}

\resumo{(Abstract in Portugeuse)}{(Abstract in English)}

\section{(Section Head} ... Body of Article ...
...
\bibliografia{(name of .bib file)} with the name of the .bib
file being the one to be used for that article.

(optional)
\appendix
appendix text
\end{article}
%%%%%%%%%%%%%%%%%%%%%%%

+++++++++++++++++++++++++++++++++++++++++++++++++
4) Use rbe-root.tex to make an 
an entry for each article, ie,

%%%%%%%%
\documentclass{rbe}
%% select first page number here:
\firstpage{305}
%% when you want the article name to appear
%% on the running foot. Comment out for final version
%% of journal.
\printcurrfilename

\begin{document}
\include{notas}
\include{article1}
\include{article2}
\include{article3}
\include{article4}
etc.
\end{document}
%%%%%%%%

Run PDFlatex on rbe-root.tex file several times.
This will produce the table of contents for the beginning
of the article, and send info to the article aux file for
making the article bibliography, and will send information out to the
summary.sum file to be used for summary and journal cover.

+++++++++++++++++++++++++++++++++++++++++++++++++
5) Make the bibliographies for each article:

(You might insist to the authors that they copy the
contents of their .bbl file into their article. This
will avoid the problem of keeping track of their .bib
and .bbl files)

Background:
\bibliografia{(filename)} should be in the article before
the appendix. (filename) is the .bib file used for citations
in this article

The \cite{} commands through the article will send information
to the auxilary file, and will then be used to make up
the bibliography.

For example, \bibliografia{1298} will use 1298.bib
to make up the bibliography. The \cite commands will
take terms from 1298.bib.

To do:
After PDFlatex has been run on the root file several times,
run the bibtex program on each of the articles:
bibtex article1, bibtex article2, bibtex article3, etc.
This will produce article1.bbl, article2.bbl, etc.

Then run PDFlatex on rberoot.tex several times more
and the bibliographies will appear in the articles,
and the \cite{} commands will produce citations.

+++++++++++++++++++++++++++++++++++++++++++++++++
5) To make the summary:

Run PDFLaTeX on summary.tex several times. 
Summary.sum and currinfo.tex will be input and their
information will be used without any need for user
intervention.

The folha-dados page will also be produced.
+++++++++++++++++++++++++++++++++++++++++++++++++
6) Make cover:

Run PDFLaTeX on rbecover.tex

+++++++++++++++++++++++++++++++++++++++++++++++++
7) To get rid of extraneous files, delete *.aux, *.log,
*.toc, *.bbl, *.blg
======================================================

Things you might want to change:

In rbe.cls:

\bottomlineheight=??pt
You can change this dimension if you want the line
at the bottom of the page to be thicker or thinner.

%% will keep single from previous paragraph from appearing on
%% top of the page, or single line from a new paragraph from appearing
%% at the bottom of a page. Delete if you don't want to follow this
%% convention

\clubpenalty=10000
\widowpenalty=10000

%% \arraystretch will make a little extra space above and below content in
%% table lines-- will make tables look less crowded and more readable.
%% 1.2 means 120 percent of the original space above and below line 
%% contents.

\def\arraystretch{1.2}



%% rbe.cls, version Mar 5, 2009

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{rbe}[2009/03/05 Standard LaTeX rbe class]

\newdimen\bottomlineheight
\bottomlineheight=18pt

\ProcessOptions\relax
%%% .sty files added here ==>> %%%%%%%%%%%%
\LoadClass[10pt,article,final,twoside,showtrims]{memoir}

%%% Patches para o pacote memoir: ocasionalmente atualizada pelo autor
\usepackage{mempatch}

%% bophook includes definition for  \AtBeginPage, used below
\usepackage{bophook}
\usepackage{multicol}
\usepackage{amsmath,amssymb, amsthm}
\usepackage[pdftex]{color,graphicx}
\usepackage[comma,authoryear]{natbib}
\usepackage{ctable,supertabular}

%% Bring in Babel with these two options, use \currlanguage{portuges,english}
%% at the start of every article
\usepackage[portuges,english]{babel}

%%% Faz com que o primeiro paragrafo de uma secao tenha identacao
\usepackage{indentfirst}

%%% Para marcar emails, urls, etc.
\usepackage{url}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}

%% for fonts, in case author uses \scalefont{.85} (or other percent
%% of full size font) in body of article. Used for tables occassionally

%% apparently for Brazilian (?)
\usepackage{icomma}

%%%%%%% <<<=== end of .sty files added in %%%%%%%%%%%%%%

%% [STOCK SIZE]: isto s� � v�lido para um stock size > que o trimmed size
\setstocksize{297mm}{210mm}
\settrimmedsize{260mm}{200mm}{*}
\settrims{0.5cm}{0.5cm}

\setulmarginsandblock{3cm}{2cm}{*}
\setlrmarginsandblock{2.5cm}{3cm}{*}

\setheadfoot{2cm}{1.0cm}
\setheaderspaces{*}{1cm}{*}

%% NUMERA��O DE P�GINAS

\newcounter{volume}
\newcounter{number}

\usethanksrule

%%% FORMATA��O DE P�GINA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (commands found in memoir.cls)
\newif\iftemp
\def\printcurrfilename{\global\temptrue}
%% Changed to use \currfilename-firstpage and \currfilename-lastpage 
%% Those numbers are set with \begin{article} \end{article}
\newcommand{\inforodape}{\iftemp(Article source: \currfilename.tex)\hfill\fi RBE \quad Rio de Janeiro \quad v.~\the\c@volume\ n.~\the\c@number\ /\ p.~\expandafter\csname \currfilename-firstpage\endcsname--%
\expandafter\csname \currfilename-lastpage\endcsname\quad \data}

\makepagestyle{rbe}
\makerunningwidth{rbe}{\textwidth}
\makeheadrule{rbe}{\textwidth}{\normalrulethickness}
\newcommand{\headerauthor}[1]{\gdef\@headerauthor{#1}}
\newcommand{\headertitle}[1]{\gdef\@headertitle{#1}}
\makeheadposition{rbe}{center}{center}{flushleft}{flushleft}

\makeevenhead{rbe}{\@headerauthor}{}{
\openin1 images/logo-fgv-simbolo.pdf \ifeof1 
(FGV Symbol)\else
\includegraphics[height=0.6cm]{images/logo-fgv-simbolo.pdf}
\fi}

\makeoddhead{rbe}{
\openin1 images/logo-rbe.pdf \ifeof1 
(RBE Logo)\else
\includegraphics[height=0.6cm]{images/logo-rbe.pdf}\fi}{}{\@headertitle}

%% infordape, text with \the\c@volume etc
\makeevenfoot{rbe}{\ffooter \inforodape}{}{}
\makeoddfoot{rbe}{}{}{\ffooter \inforodape}
\pagestyle{rbe}

%% Running heads for title page:
\copypagestyle{title}{rbe}
\makeevenhead{title}{}{}{}
\makeoddhead{title}{
\openin1 images/logo-rbe.pdf \ifeof1 (RBE Logo)\else
\includegraphics[height=0.6cm]{images/logo-rbe.pdf}\fi}{}{}
\makeevenfoot{title}{}{}{}
\makeoddfoot{title}{}{}{\ffooter \inforodape}

%% Notes on page and trim size
%%% [SOCK SIZE]: estes valores s�o para um stock size > trimmed size + trims de 0.5 cm
%%% Altura
%%% 260 - 8         = 252
%%% 252 + 5 (trims) = 257

%%% Largura
%%% 25 + 5 (trims) = 30
%%% 5              = 5

%%% Estes s�os os valores para um stock size == trimmed size
%%% Altura
%%% 260 - 8         = 252

%%% Largura
%%% 25 = 30

%%%%%%%%%%%%%%%%%%
%% Puts pagenumber and line at bottom of page
%% Height of line determined by dimension \bottomlineheight
%% which is set at the top of this .cls file

\def\xempty{empty}

 \AtBeginPage{%
%% to make sure this definition is used, since user may bring
%% in other packages
\renewcommand{\qedsymbol}{$\blacksquare$}
 \setlength{\unitlength}{1mm}
 \checkoddpage
 \ifoddpage
%% No page number on empty pages:
\ifx\@specialstyle\xempty\else
 \put(30, -257){\makebox(0,0)[tl]{\vrule width 18cm 
height \bottomlineheight
}}%
 \put(188, -252){\makebox(0,0)[tl]{\fpagina\thepage}}
\fi%% end test to see if this page is supposed to be empty
 \else
%% No page number on empty pages:
\ifx\@specialstyle\xempty\else
 \put(0, -257){\makebox(0,0)[tl]{\vrule width 18cm 
height \bottomlineheight
}}%
 \put(18, -252){\makebox(0,0)[tl]{\fpagina\thepage}}
 \fi\fi
 }

%%%%% Font Information %%%%%%%%%%%%%%%%%%
%%% bo1 == oranda
%%% pr3 == rotis
%%% bun == zurich (aka univers)

\newcommand{\ftitulo}{\fontfamily{bun}\fontseries{b}\fontsize{24pt}{28pt}\selectfont}
\newcommand{\fnota}{\fontfamily{bun}\fontseries{b}\fontsize{24pt}{25pt}\selectfont}
\newcommand{\fautores}{\fontfamily{bo1}\fontseries{m}\fontsize{15pt}{16pt}\selectfont}
\newcommand{\fresumo}{\fontfamily{bo1}\fontseries{m}\fontsize{10pt}{14pt}\selectfont}
\newcommand{\fitem}{\fontfamily{bo1}\fontseries{b}\fontsize{12pt}{14pt}\selectfont}
\newcommand{\fsubitem}{\fontfamily{bo1}\fontseries{b}\fontsize{12pt}{14pt}\selectfont}
\newcommand{\foutrassubd}{\fontfamily{bo1}\fontseries{m}\fontshape{it}\fontsize{10pt}{11pt}\selectfont}
\newcommand{\fcorpo}{\fontfamily{bo1}\fontseries{m}\selectfont}
\newcommand{\frodape}{\fontfamily{bo1}\fontseries{m}\fontsize{8pt}{10pt}\selectfont}
\newcommand{\fpagina}{\fontfamily{bun}\fontseries{b}\fontsize{12pt}{13pt}\selectfont}
\newcommand{\ffooter}{\fontfamily{bun}\fontseries{m}\fontsize{8pt}{10pt}\selectfont}

%%% T�TULO DO ARTIGO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\droptitle}{-4em}    % o memoir for�a um determinado espa�amento
                                % antes do t�tulo
                                % ent�o temos que dar -4em para compensar
                                % e fazer com o que o t�tulo fique no topo
                                % da p�gina
\pretitle{\begin{flushleft}\ftitulo}
\posttitle{\vskip 1em\end{flushleft}}

%%% AUTORES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\preauthor{\begin{flushleft}\fautores}
\postauthor{\vskip 1em\end{flushleft}}

%%% AGRADECIMENTOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\thanksmarkwidth}{0.25cm}
\setlength{\thanksmarksep}{0pt}

%%% RESUMO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\abstractnamefont}{\fresumo}
\renewcommand{\abstracttextfont}{\fresumo}

\newcommand{\resumo}[2]{%
\renewcommand{\abstractname}{}
\abstractrunin
\setlength{\absleftindent}{20mm}
\setlength{\absrightindent}{25mm}
%\setlength{\absparindent}{0mm}
\setlength{\absparsep}{1em}
\begin{abstract}
\selectlanguage{portuges}
#1

\emph{\selectlanguage{english}
#2}
\end{abstract}}

%%% NOTAS DE RODAP� %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\footmarkwidth}{0.25cm}
\setlength{\footmarksep}{0pt}
\setlength{\footparindent}{0pt}
\setlength{\footnotesep}{1em}

\newcommand{\footnoterule}{\hrule height 0.3mm width 0.33\columnwidth}
\renewcommand{\foottextfont}{\frodape}

%%% CORPO DO TEXTO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\sfdefault}{bo1}
\renewcommand{\rmdefault}{bo1}
\renewcommand{\normalfont}{\fcorpo}
\setlength{\parindent}{0.5cm}

%%% TABELAS E FIGURAS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\captiondelim{~--~}
\captionnamefont{\bfseries}

%%% FONTES MATEM�TICAS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% {bo1}{m}{n}
%% {bo1}{m}{it}
%% {bo1}{m}{sl}

%% Desta maneira, n�meros e operadores matem�ticos
%% saem na Oranda e vari�veis e s�mbolos gregos (que
%% n�o existem na Oranda) saem normalmente. Usar a oranda
%% para letras n�o funciona muito bem porque de alguma 
%% maneira o TeX erra o espa�amento

% \SetSymbolFont{operators}     {normal}{OT1}{bo1}{m}{n}

%%% SE��ES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\secheadstyle}{\fitem\MakeUppercase}
\renewcommand{\subsecheadstyle}{\fsubitem}
\renewcommand{\subsubsecheadstyle}{\foutrassubd}
\setsecnumdepth{subsubsection}
\setsubsecindent{\parindent}
\setsubsubsecindent{\parindent}
\setsecnumformat{\csname the#1\endcsname.~}
\renewcommand{\thesection}{\arabic{section}}

\renewcommand{\appendix}{\par 
\setcounter{figure}{0}
\setcounter{table}{0}
\setcounter{equation}{0}
\def\thefigure{\@Alph\c@section-\arabic{figure}}
\def\thetable{\@Alph\c@section-\arabic{table}}
\def\theequation{\@Alph\c@section-\arabic{equation}}
  \setcounter{section}{0}
  \gdef\thesection{\@Alph\c@section}}


%%% SUM�RIO, PALAVRAS-CHAVE E CLASSIFICA��O %%%%%%%%%%%%

\newcounter{currentsection}

\renewcommand{\tableofcontents}{\vskip-12pt\begingroup
\c@currentsection=0
\fresumo \footnotesize\noindent 
\hbox to 21mm{\hfill\bfseries \contentsname:}\hskip10pt\vtop{\fresumo\footnotesize\bfseries
\raggedright\advance\hsize-2in
\noindent
\openin1 \currfilename.toc \ifeof1 
\else
\input \currfilename.toc
\fi
\vskip2pt
}
\endgroup 
\expandafter\newwrite\csname\currfilename toc\endcsname
\immediate\openout\csname\currfilename toc\endcsname
=\currfilename.toc}

\renewcommand\l@section[1]{\addtocounter{currentsection}{1}#1%
\expandafter\ifx\csname\currfilename-themaxsection\endcsname\relax\else
\ifnum\thecurrentsection=\expandafter\csname\currfilename-themaxsection\endcsname.\else;\fi\fi }

\renewcommand\l@chapter[2]{}
\renewcommand\l@subsection[2]{}
\renewcommand\l@subsubsection[2]{}
\renewcommand{\numberline}[1]{#1.~}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Using commands from Memoir.cls

\trimLmarks
\typeoutlayout
\checkandfixthelayout
\fixpdflayout

%%%%%%%%%%%%% Utils

\newcommand{\email}[1]{E-mail: \protect\url{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Additions by Amy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Date, Volume, and number of issue written in currinfo.tex 
%% (so that we can access this info for articles, summary, and cover)
%%
%% Bring in currinfo.tex, if it exists; else give
%% error message:
\openin1 currinfo.tex \ifeof1 
\typeout{^^J^^JWarning:
currinfo.tex not found^^J^^J
Please include the following text in a file named currinfo.tex:^^J
\string\setcounter{volume}{ ( number of current volume ) }^^J
\string\setcounter{number}{ ( number of this issue ) }^^J
\string\newcommand{\string\data}{ (months and year of this issue) }
^^J
^^J
}\def\data{}%% to avoid error message
\else
\input currinfo
\fi


%% Only section heads will be sent to toc:
\setcounter{tocdepth}{1}

\newif\ifingles
\def\xportuges{portuges}

\def\currlanguage#1{\def\one{#1}
\ifx\one\xportuges\global\inglesfalse\selectlanguage{portuges}\else
\global\inglestrue\selectlanguage{english}\fi}

%%%%%%%
%% switch language according to \currlanguage{} set at beginning of article:
\def\palavraschave{\vskip2pt\setbox0=\vbox\bgroup\raggedright
\fresumo\footnotesize\bfseries 
\advance\hsize by -2in}

\def\endpalavraschave{\vrule depth4pt width0pt\vskip1sp\egroup
\noindent\hbox to 21mm{\hfill\fresumo\footnotesize\bfseries 
\ifingles Keywords:\else Palavras-chave:\fi}\hskip10pt
\vtop{\advance\hsize -2in\unvbox0}}

\gdef\classificacao{\vskip2pt\setbox0=\vbox\bgroup\raggedright
\fresumo\footnotesize\bfseries
\advance\hsize by -2in}

\gdef\endclassificacao{\egroup\bgroup\fresumo\footnotesize\bfseries
\noindent\hbox to 21mm{\hfill\ifingles JEL Code:\else
C�digos~JEL:\fi}\hskip10pt\vtop{\advance\hsize-2in \unvbox0}\egroup}


%%% DEFINI��ES, TEOREMAS, ETC.%%%%%%%%%

%% These theorem type environments change language according to
%% the \currlanguage{} command at the beginning of each article.

\newtheorem{theorem}{\ifingles Theorem\else Teorema\fi}[section]
\newtheorem{lemma}[theorem]{\ifingles Lemma\else Lema\fi}
\newtheorem{definition}[theorem]{\ifingles Definition\else Defini��o\fi}
\newtheorem{proposition}[theorem]{\ifingles Proposition\else Proposi��o\fi}
\newtheorem{assumption}[theorem]{\ifingles Assumption\else Conjectura\fi}
\newtheorem{corollary}[theorem]{\ifingles Corollary\else Corol�rio\fi}
\newtheorem{example}{\ifingles Example \else Exemplo\fi}[section]
\newtheorem{remark}{\ifingles Remark\else Nota\fi}[section]

%% Used for resetting counters, and for starting on odd numbered page;
%% sends first page number and last page number to .aux file
\def\article{\ifodd\c@page\else\null\thispagestyle{empty}\newpage\fi
\reset
\write\@auxout{\string\expandafter\string\gdef\string\csname\space \currfilename-firstpage\string\endcsname{\the\c@page}}}

\newcount\lastpagefix
\let\endarticle\relax

%% We can add more counters to reset here if necessary
\def\reset{\refstepcounter{chapter}%% in case this resets any counters
\global\c@theorem=0
\global\c@example=0
\global\c@remark=0
\global\c@figure=0
\global\c@table=0
\global\c@equation=0
\gdef\thesection{\arabic{section}}
%\def\thefigure{\arabic{figure}}
%\def\thetable{\arabic{table}}
%\def\theequation{\arabic{equation}}
\global\c@currentsection=0
\global\newmaxsection=0
\emptythanks\global\c@footnote=0\title{}\author{}
\headerauthor{}\headertitle{}}

%% automates finding \maxsection for each article:

%% default, so we don't get error message first time
%% LaTeX is run on file.
\def\themaxsection{0}

%% modification of existing \section command to add newmaxsection
%% counter which is expanded when definition for \maxcaption is
%% sent to the aux file for the particular article.

%% Every section will send a new definition for \themaxsection
%% to the .aux file; only the final one will used when the article table
%% of contents is made up.
\newcount\newmaxsection
\long\def\section{\global\advance\newmaxsection by1
\immediate\write\@auxout{\string\expandafter\gdef\string\csname\space
\currfilename-themaxsection\string\endcsname{%
\the\newmaxsection}}
\sechook 
\@startsection {section}{1}{\secindent }{\beforesecskip }{\aftersecskip }
{\normalfont \secheadstyle }}


%%%%%%%%%%%%%%
%% Make summary.sum to receive title/author/page number
%% information from each article. title/author/page number is used
%% to produce summary when pdflatex is run on summary.tex; 
%% title/author is used to produce Cover when pdflatex is run on rbecover.tex

\newwrite\summaryfile
\immediate\openout\summaryfile=summary.sum

%% Definition of \@maketitle
%% taken from memoir.cls; added \write to send thetitle 
%% and theauthor and page number to summary.sum

\newcount\articlenumber

\renewcommand{\@maketitle}{%
\global\advance\articlenumber by1 %% <== added
  \let\footnote\@mem@titlefootkill
  \ifdim\pagetotal>\z@
    \newpage
  \fi
  \null
  \vskip 2em%
        \vspace*{\droptitle}
  \maketitlehooka
  {\@bspretitle \@title 
%% added ==>
%% \string keeps command following from expanding so that the
%% result in summary.sum will be
%% \expandafter\gdef\csname article(current article number)-title\endcsname
%% {current title} -- Making a unique definition for every article title
%%
\write\summaryfile{\string\expandafter\string\gdef\string\csname\space
article\the\articlenumber-title\string\endcsname{\thetitle}}
%%
%% Here we are sending a definition of the \maxarticlenum to the summary file.
%% Each article will contribute a new definition; but the final one is the
%% one that will be used, since TeX uses only the most recent definition.
%% The \articlenumber expand immediately, the \gdef and \maxarticlenum
%% appear in the summary.sum file without expanding. Results:
%% \gdef\maxarticlenum{(current article number)}
\write\summaryfile{\string\gdef\string\maxarticlenum{\the\articlenumber}}%
%%
%% Here we send the page number of the first page of the article to summary.sum
%% \expandafter\gdef\csname article (curr article number)-page\endcsname
%% {(currentpage number)} is what will appear in summary.sum
%%
\write\summaryfile{\string\expandafter\string\gdef\string\csname\space 
article\the\articlenumber-page\string\endcsname{\the\c@page}}%
%% <== end addition
\@bsposttitle}
  \maketitlehookb
  {\@bspreauthor \@author 
%% added ==>
%% This sends the author name to the summary.sum file in a form that
%% looks like this: \expandafter\gdef\csname article(currarticle number)-author
%% {(the author name(s))}
%%
\write\summaryfile{\string\expandafter\string\gdef\string\csname\space article\the\articlenumber-author\string\endcsname{\theauthor}}%
%% <== end addition
\@bspostauthor}
  \maketitlehookc
  {\@bspredate %\@date 
\@bspostdate}
  \maketitlehookd
  \par
  \vskip 1.5em}

%%% BIBLIOGRAFIA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Natbib uses bibsection
\long\def\@memb@bchap{\chapter*{\ifingles BIBLIOGRAPHY\else BIBLIOGRAFIA\fi}\bibmark 
\prebibhook} 

\renewcommand{\bibsection}{\section*{\ifingles BIBLIOGRAPHY\else BIBLIOGRAPHIA\fi}\prebibhook}


\def\bibliografia#1{\ifingles \bibliographystyle{rbe-en}\else
\bibliographystyle{rbe-pt}\fi\bibliography{#1}}

%% Make bibliography look for .bbl file with same name as article,
%% not root file:

%% Change definition of LaTeX \include command:
\def\include#1{\relax
%% added, so that we can look for \currfilename when looking for the .bbl 
%% file, instead of the name of the root file.
\def\currfilename{#1}
\typeout{^^J 
=====================================================
^^J
Comments on \currfilename.tex\space follows:
^^J^^J }
%% 
  \ifnum\@auxout=\@partaux
    \@latex@error{\string\include\space cannot be nested}\@eha
  \else \@include#1 \fi}

\def\bibliography#1{\if@filesw\immediate\write\@auxout{\string\bibdata{#1}}\fi
%% Here is where \currfilename is used:
\@input@{\currfilename.bbl}}

%%% To make sure labels are unique in each issue,
%%% since authors may use the same terms, like fig-1.
%%% Now \label{tab-1} in file 1029 will yield label: 1029tab-1 and
%%% \ref[tab-1} in file 1029.tex will look for 1029tab-1

\def\label#1{\@bsphack
  \protected@write\@auxout{}%
         {\string\newlabel{\currfilename:#1}{{\@currentlabel}{\thepage}}}%
  \@esphack}

\def\ref#1{\expandafter\@setref\csname r@\currfilename:#1\endcsname\@firstoftwo{#1}}

\def\pageref#1{\expandafter\@setref\csname r@\currfilename:#1\endcsname
                                   \@secondoftwo{#1}}

\let\ltx@label\label

\let\currfilename\relax
%%%%%%%%

\def\M@sect#1#2#3#4#5#6[#7][#8]#9{%
  \ifheadnameref\M@gettitle{#8}\else\M@gettitle{#7}\fi
  \ifnum #2>\c@secnumdepth
    \let\@svsec\@empty
    \memsecinfo{#1}{}{#7}{#8}{#9}%
  \else
    \refstepcounter{#1}%
    \protected@edef\@svsec{\@seccntformat{#1}\relax}%
    \memsecinfo{#1}{\@nameuse{the#1}}{#7}{#8}{#9}%
  \fi
  \@tempskipa #5\relax
  \ifdim \@tempskipa>\z@
    \begingroup
      #6{%
      \@hangfrom{\hskip #3\relax\@svsec}%
        \interlinepenalty \@M #9\@@par}%
    \endgroup
    \csname #1mark\endcsname{#8}%
\ifnum #2=1
\expandafter\write\csname\currfilename toc\endcsname{%
\string\contentsline {#1}{%
      \ifnum #2>\c@secnumdepth \else
        \protect\numberline{\csname the#1\endcsname}%
      \fi
      #7}}\fi%
  \else
    \def\@svsechd{%
      #6{\hskip #3\relax
     \@svsec #9}%
     \csname #1mark\endcsname{#8}%
%% send section info to \currfilename instead of to root.aux
\ifnum #2=1
\expandafter\write\csname\currfilename toc\endcsname{%
\string\contentsline {#1}{%
       \ifnum #2>\c@secnumdepth \else
        \protect\numberline{\csname the#1\endcsname}%
       \fi
       #7}}\fi}%
  \fi
  \@xsect{#5}}

\def\firstpage#1{\setcounter{page}{#1}}

\def\@include#1 {%
  \clearpage
  \if@filesw
    \immediate\write\@mainaux{\string\@input{#1.aux}}%
  \fi
  \@tempswatrue
  \if@partsw
    \@tempswafalse
    \edef\reserved@b{#1}%
    \@for\reserved@a:=\@partlist\do
      {\ifx\reserved@a\reserved@b\@tempswatrue\fi}%
  \fi
  \if@tempswa
    \let\@auxout\@partaux
    \if@filesw
      \immediate\openout\@partaux #1.aux
      \immediate\write\@partaux{\relax}%
    \fi
    \@input@{#1.tex}%
\clearpage
\thispagestyle{empty}
\lastpagefix=\c@page
\advance\lastpagefix -1
\immediate\write\@auxout{\string\expandafter%
\string\gdef\string\csname\space 
\currfilename-lastpage\string\endcsname{\the\lastpagefix}}
    \@writeckpt{#1}%
    \if@filesw
      \immediate\closeout\@partaux
    \fi
  \else
    \deadcycles\z@
    \@nameuse{cp@#1}%
  \fi
  \let\@auxout\@mainaux}

%% to avoid error message, and to get TOC etc to work correctly
%% when author is making article without \include{} command:

\let\currfilename\jobname

%% will keep single from previous paragraph from appearing on
%% top of the page, or single line from a new paragraph from appearing
%% at the bottom of a page:
\clubpenalty=10000
\widowpenalty=10000

%% This will make a little extra space above and below content in
%% table lines-- will make tables look less crowded and more readable.
\def\arraystretch{1.2}

\endinput


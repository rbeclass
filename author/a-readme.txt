=============================================
Making the RBE Journal Article using PDFLaTeX
=============================================
Amy Hendrickson, TeXnology Inc,
http://www.texnology.com
amyh@texnology.com
March 3, 2009
----------------------------------

Dear Authors:

You are not expected to have either the final fonts
that will be used for this paper, nor the graphics
files that are used for the running heads. So, you
will notice that the samp1.pdf file, and the file
that you produce will have place holders for the
missing graphics, and will not have the page range
for the article printed at the bottom of the page.

Don't worry about any of this; your .tex file will
be re-run with PDFLaTeX for final production, and
the correct fonts, and graphics for the 
running heads and feet will be added.

Instructions for making your RBE journal article will be found
in the template file, rbe-tmpl.tex.

Please rename rbe-tmpl.tex and enter your text in
this file. You will see comments and advice in
the file, which will serve as the only documentation
you will need.

You may notice that the file "summary.sum" is made
when you run PDFLaTeX on your file. You can ignore
this file-- It will be used when the entire journal
is assembled.

==================================================
Description of Files in this package:
======================
Basic class file
rbe.cls

Template file for article:
rbetmpl.tex

For bibliography styles:
rbe-en.bst %% for english bibliography
rbe-pt.bst %% for portugeuse bibliography

-----------------------
Sample Files:

Sample .tex file
samp1.tex

Sample article results, after running pdflatex on samp1.tex
samp1.pdf

Sample bib data file
samp1.bib

Sample .bbl file, made by running bibtex on samp1
samp1.bbl

Sample illustration file:
figsamp.pdf

====================

% This is the general form of the article:
\documentclass{rbe}
\begin{document}
\begin{article}

%% choose one:
  \currlanguage{portuges} or
  \currlanguage{english} 

\title{}
\author{}
\maketitle

% for running heads:
\headerauthor{}
\headertitle{}

\tableofcontents

\begin{palavraschave}
...
\end{palavraschave}

\begin{classificacao}
...
\end{classificacao}

\resumo{(Abstract in Portugeuse)}{(Abstract in English)}

\section{(Section Head} ... Body of Article ...
...
\bibliografia{(name of .bib file)} with the name of the .bib
file being the one to be used for that article.

%% optional \appendix
%% Numbering following \appendix will include appendix letter;
%% sections will be lettered instead of numbered
\end{article}
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%

See Samp1.tex for sample file, and samp1.pdf for
results


Thank you, and enjoy!


